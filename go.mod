module github.com/siongui/paligo

go 1.17

require (
	github.com/siongui/godom v0.0.0-20200920152407-31a6de96fdf6
	github.com/siongui/goef v0.0.0-20210610184109-d3b60554c5f3
	github.com/siongui/gopalilib v0.0.0-20211007223423-f30f0cb79ff4
	github.com/siongui/gopaliwordvfs v0.0.0-20200117014540-dd746a8f51e6
	github.com/siongui/gopherjs-input-suggest v0.0.0-20200723202201-f43790c91d84
	github.com/siongui/gtmpl v0.0.0-20200716010746-db0e14d247f9
	github.com/siongui/pali-transliteration v0.0.0-20200916143527-6049b271da36
)

require (
	github.com/chai2010/gettext-go v1.0.2 // indirect
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20210923143318-357ed63a84fb // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/neelance/astrewrite v0.0.0-20160511093645-99348263ae86 // indirect
	github.com/neelance/sourcemap v0.0.0-20200213170602-2833bce08e4c // indirect
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/siongui/go-succinct-data-structure-trie v0.0.0-20210926213055-1e96df0b792f // indirect
	github.com/siongui/gojianfan v0.0.0-20210926212422-2f175ac615de // indirect
	github.com/spf13/cobra v1.2.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/winhtaikaung/Rabbit-Go v0.0.0-20190314045429-213403fa9212 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20210924151903-3ad01bbaa167 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20210925032602-92d5a993a665 // indirect
	golang.org/x/term v0.0.0-20210916214954-140adaaadfaf // indirect
	golang.org/x/text v0.3.6 // indirect
	golang.org/x/tools v0.1.6 // indirect
)
